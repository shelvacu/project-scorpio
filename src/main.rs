#![feature(toowned_clone_into, nll, type_ascription)]
#![allow(dead_code)]

extern crate fftw; //for fourier transforms
extern crate audrey; //for wav, flac decoding
extern crate pbr; //for showing progress bars
extern crate crossbeam; //for scoped threads
extern crate spmc; //single producer multiple consumer inter-thread comm
//extern crate simd; //single-instruction-multiple-data: for SPEEEED
extern crate simplemad; //mp3 decoding library

use std::env;
use std::vec::Vec;
use std::ops::Range;
//use std::time::Duration;
use std::fs::File;
use std::path::Path;
use std::thread;
use std::cmp::{max,min};

use fftw::types::Flag;
use fftw::plan::R2CPlan;

use pbr::ProgressBar;

use audrey::hound;

fn complex_as_f32(cslice: &[fftw::types::c32]) -> &[f32] {
    use std::slice;
    return unsafe {
        slice::from_raw_parts(
            cslice.as_ptr() as *const f32,
            cslice.len() * 2
        )
    };
}

fn f32_as_complex(fslice: &[f32]) -> &[fftw::types::c32] {
    use std::slice;
    if fslice.len() % 2 != 0 {
        panic!("fslice is of odd size")
    }
    return unsafe {
        slice::from_raw_parts(
            fslice.as_ptr() as *const fftw::types::c32,
            fslice.len() / 2
        )
    };
}

fn make_array<T:Default>(size:usize) -> Vec<T> {
    let mut res = Vec::<T>::with_capacity(size);
    for _ in 0..size {
        res.push(Default::default());
    }
    return res;
}

fn open_file(filename:String) -> (u32,audrey::read::BufFileReader) {
    let reader = audrey::read::open(filename)
        .expect("Could not read file");

    let description = reader.description();

    eprintln!("{:#?}", description);

    if description.channel_count() != 1 {
        panic!("Must use an audio file with exactly one channel");
    }

    return (description.sample_rate(), reader);
}

fn read_file(mut reader:audrey::read::BufFileReader) -> Vec<f32> {
    //let mut reader = open_file(filename);

    let samples:Vec<f32> = reader.samples::<f32>().map(|x| x.unwrap()).collect();

    return samples;
}

fn main() {
    //let args: Vec<String> = env::args().collect();

    eprintln!("Project Scorpio!");

    match env::var("WHICH_STEP") {
        Err(env::VarError::NotPresent) => panic!(),
        Ok(step) => {
            if step == String::from("step1.1") {
                step1(1)
            } else if step == String::from("step1.2") {
                step1(2)
            } else if step == String::from("step2") {
                step2()
            } else if step == String::from("step3") {
                step3()
            } else {
                panic!("thing {:#?}", step);
            }
        },
        val => panic!("unrecognized {:#?}", val),
    }
    
    /*
    if args[0] == String::from("step1") { step1() }
    else if args[0] == String::from("step2") { step2() }
    else {panic!("bad name")}~*/
}

fn step2() {
    let args: Vec<String> = env::args().collect();
    let mut reader = hound::WavReader::open(args[1].clone()).unwrap();

    let mut iter = reader.samples::<f32>();
    let mut i = 0;
    let mut minimum = (i,iter.next().unwrap().unwrap());
    i += 1;
    for r in iter {
        let s = r.unwrap();
        if s < minimum.1 {
            minimum = (i,s)
        }
        i += 1;
    }
    println!("min {:#?}", minimum);
}

fn find_min_vals<'a, I:Iterator<Item = &'a f32>>(
    within:I,
    excl:usize,
    how_many:usize
) -> Vec<(usize, f32)> {
    let mut mins = Vec::<(usize, f32)>::with_capacity(how_many+1);
    let mut i:usize = 0;
    for thing in within {
        match (&mut mins).into_iter().find(|v| (v.0 - excl) < i && i < (v.0 + excl)) {
            Some(val) => {
                if thing < &val.1 {
                    *val = (i, *thing);
                }
            },
            None => {
                if (&mins).into_iter().any(|v| thing < &v.1) || mins.len() < how_many {
                    mins.push((i, *thing));
                }

                if mins.len() > how_many {
                    mins.sort_unstable_by(|a,b| a.1.partial_cmp(&b.1).unwrap());
                    mins.pop();
                }
            },
        }

        i += 1;
    }
    mins.sort_unstable_by(|a,b| a.1.partial_cmp(&b.1).unwrap());
    return mins;
}

fn find_max_vals<'a, I:Iterator<Item = &'a f32>>(
    within:I,
    excl:usize,
    how_many:usize
) -> Vec<(usize, f32)> {
    let mut maxs = Vec::<(usize, f32)>::with_capacity(how_many+1);
    let mut i:usize = 0;
    for thing in within {
        match (&mut maxs).into_iter().find(|v| (v.0 - excl) < i && i < (v.0 + excl)) {
            Some(val) => {
                if thing > &val.1 {
                    *val = (i, *thing);
                }
            },
            None => {
                if (&maxs).into_iter().any(|v| thing > &v.1) || maxs.len() < how_many {
                    maxs.push((i, *thing));
                }

                if maxs.len() > how_many {
                    maxs.sort_unstable_by(|a,b| a.1.partial_cmp(&b.1).unwrap().reverse());
                    maxs.pop();
                }
            },
        }

        i += 1;
    }
    maxs.sort_unstable_by(|a,b| a.1.partial_cmp(&b.1).unwrap().reverse());
    return maxs;
}

fn find_min_excl<T:PartialOrd<T>>(
    within:&[T],
    excl:&mut [Range<usize>]
) -> usize {
    let mut min:Option<(usize,&T)> = None;
    excl.sort_by_key(|r| r.start);
    let mut nums = vec![0usize];
    for r in excl {
        nums.push(r.start);
        nums.push(r.end);
    }
    nums.push(within.len());

    let mut ranges_to_search:Vec<Range<usize>> = Vec::new();
    let mut len = 0;
    for slice in nums.chunks(2) {
        ranges_to_search.push(slice[0]..slice[1]);
        len += slice[1] - slice[0];
    }
    let mut num = 1;
    let mut pbar = ProgressBar::on(std::io::stderr(), (len/1024) as u64);

    for range in ranges_to_search {
        for i in range {
            match min {
                Some((_,old_val)) => {
                    if &within[i] < old_val {
                        min = Some((i, &within[i]))
                    }
                },
                None => min = Some((i, &within[i])),
            }
            num += 1;
            if num % 1024 == 0 {
                pbar.inc();
            }
        }
    }
    
/*    while i < within.len() {
        for range in excl {
            if range.start == i && range.end > range.start {
                let oldi = i;
                i = range.end;
                pbar.add((i-oldi) as u64);
                continue;
            }
        }
    }*/
    pbar.finish();
    match min {
        None => panic!("excluded entire array"),
        Some((i,_)) => return i,
    }
}

fn step3() {
    let args: Vec<String> = env::args().collect();
    let input_podcast_fn = args[3].clone();
    let output_podcast_fn = args[4].clone();
    let mut reader = hound::WavReader::open(args[1].clone()).unwrap();
    let mut other_reader = hound::WavReader::open(args[2].clone()).unwrap();
    let sr = reader.spec().sample_rate as usize;
    let samples:Vec<f32> = reader.samples::<f32>().map(|r| r.unwrap()).collect();
    let other_samples:Vec<f32> = other_reader.samples::<f32>().map(|r| r.unwrap()).collect();
    assert_eq!(other_reader.spec().sample_rate as usize, sr);
    eprintln!("Got samples");
    /*let mut exclusions:Vec<std::ops::Range<usize>> = Vec::new();

    let min1 = find_min_excl(&samples, &mut exclusions);
    exclusions.push(min1-(sr)..min1+(sr));
    eprintln!("got min1");
    
    let min2 = find_min_excl(&samples, &mut exclusions);
    exclusions.push(min2-(sr)..min2+(sr));
    eprintln!("got min2");
    
    let min3 = find_min_excl(&samples, &mut exclusions);
    exclusions.push(min3-(sr)..min3+(sr));
    eprintln!("got min3");

    let min4 = find_min_excl(&samples, &mut exclusions);*/

    let mins = find_max_vals((&samples).into_iter(), sr, 4);

    eprintln!(
        "using {} {} {}, ignoring {}",
        mins[0].0 as f32/sr as f32,
        mins[1].0 as f32/sr as f32,
        mins[2].0 as f32/sr as f32,
        mins[3].0 as f32/sr as f32,
    );

    /*let mut o_exclusions:Vec<std::ops::Range<usize>> = Vec::new();
    let o_min1 = find_min_excl(&other_samples, &mut o_exclusions);
    o_exclusions.push(o_min1-(sr)..o_min1+(sr));
    eprintln!("got o_min1");
    
    let o_min2 = find_min_excl(&other_samples, &mut o_exclusions);
    o_exclusions.push(o_min2-(sr)..o_min2+(sr));
    eprintln!("got o_min2");
    
    let o_min3 = find_min_excl(&other_samples, &mut o_exclusions);
    o_exclusions.push(o_min3-(sr)..o_min3+(sr));
    eprintln!("got o_min3");

    let o_min4 = find_min_excl(&other_samples, &mut o_exclusions);
    */

    let o_mins = find_max_vals((&other_samples).into_iter(), sr, 4);
    eprintln!("{:?}", o_mins);

    eprintln!(
        "using {} {} {}, ignoring {}",
        o_mins[0].0 as f32/sr as f32,
        o_mins[1].0 as f32/sr as f32,
        o_mins[2].0 as f32/sr as f32,
        o_mins[3].0 as f32/sr as f32,
    );

    
    let ad_start_len_samples = 38525;
    let asls = ad_start_len_samples;

    let mut secs = vec![
        mins[0].0,
        mins[1].0,
        mins[2].0,
        o_mins[0].0,
        o_mins[1].0,
        o_mins[2].0
    ];
    secs.sort_unstable();

    eprintln!("sex {:?}", secs);

    let (ipsr,mut in_podcast_reader) = open_file(input_podcast_fn);
    assert_eq!(ipsr as usize, sr);
    let spec = hound::WavSpec {
        channels: 1,
        sample_rate: 44100,
        bits_per_sample: 32,
        sample_format: hound::SampleFormat::Float,
    };
    let mut writer = hound::WavWriter::create(output_podcast_fn, spec).unwrap();
    let mut i = 0;

    //ENGAGE grasping pedipalps (SCORPION CLAWS)
    for maybe_sample in in_podcast_reader.samples::<f32>() {
        let sample = maybe_sample.unwrap();

        if  (i > secs[0]+asls && i < secs[1]) ||
            (i > secs[2]+asls && i < secs[3]) ||
            (i > secs[4]+asls && i < secs[5])
        {
            //skip
        }else{
            writer.write_sample(sample*50.0).unwrap();
        }

        i += 1;
    }
}

const PI:f32 = 3.141592653;

fn hanning(size:usize) -> Vec<f32>{
    let mut res:Vec<f32> = Vec::with_capacity(size);
    for n in 0..size {
        res.push(
            (
                1.0-( ((n as f32)*2.0*PI)/(size - 1) as f32 ).cos()
            )/2.0
        );
    }
    return res;
}

fn multiply_into(hanning:&[f32], into:&mut [f32]){
    if hanning.len() != into.len() {
        panic!("into must be same len");
    }
    for i in 0..hanning.len() {
        into[i] *= hanning[i];
    }
}

#[derive(Debug)]
pub enum Mode {
    HI,
    Cortex,
}

fn read_mp3(
    mp3_name:String
) -> (std::thread::JoinHandle<()>,std::sync::mpsc::Receiver<f32>) {
    use simplemad::Decoder;
    
    let (samples_tx, samples_rx) = std::sync::mpsc::channel();
    let handle = thread::spawn(move || {
        let input_podcast_path = Path::new(&mp3_name);
        let ip_file = File::open(&input_podcast_path).unwrap();
        let ip_decoder = Decoder::decode(ip_file).unwrap();

        for res in ip_decoder {
            match res {
                Err(e) => eprintln!("MAD decode err {:#?}", e),
                Ok(frame) => {
                    for i in 0..frame.samples[0].len() {
                        let avg = (frame.samples[0][i].to_f32() + frame.samples[1][i].to_f32())/2.0;
                        samples_tx.send(avg).unwrap();
                    }
                }
            }
        }
    });

    return (handle, samples_rx);
}
/*
fn all_step(
    args:Vec<String>,
    window_size:usize,
    overlap:usize,
    mode:Mode
) {
    if let Mode::HI = mode {
        //all is well
    } else { panic!("Not implemented"); }
    let input_podcast_fn = args[0];
    let ad_start_bloop = args[1];
    let ad_end_bloop = args[2];
    let output_podcast_fn = args[3];

    let (_handle, ip_samples_rx) = read_mp3(input_podcast_fn);
    //let (h_sr, h_rr) = open_file(input_podcast_fn);
    let mut needle_files = Vec::new();
    for name in (ad_start_bloop, ad_end_bloop) {
        let (sr, rr) = open_file(name);
        if sr != 44100 { panic!("needles must have sample rate of 44100hz") }
        needle_files.push(rr);
    }

    let mut needle_sample_vecs = Vec::new();
    for f in needle_files {
        needle_sample_vecs.push(read_file(f));
    }

    let mut all_same = true;
    for vec in &needle_sample_vecs {
        all_same = all_same && vec.len() == needle_sample_vecs[0].len()
    }
    if !all_same {
        panic!("all needles must be same length");
    }
    
    let needle_num_samples = needle_sample_vecs[0].len();
    if needle_num_samples < window_size {
        panic!("window size too big");
    }

    let hanning_window = hanning(window_size);

    let mut template_vecs = Vec::new();
    for _ in 0..num_searches {
        template_vecs.push(make_array::<fftw::types::c32>(needle_num_windows*window_size));
    }

    let mut input = make_array::<f32>(window_size);

    let shape = vec![window_size];
    let mut template_plan = fftw::plan::R2CPlan32::new(
        &shape,
        input.as_mut_slice(),
        &mut template_vecs[0][0..window_size],
        Flag::Patient | Flag::DestroyInput
    ).unwrap();

    let num_searches = 2;

    for search_num in 0..num_searches {
        for i in 0..needle_num_windows {
            let start_idx = i*(window_size-overlap);
            let end_idx = start_idx + window_size;
            &needle_sample_vecs[search_num][start_idx..end_idx].clone_into(&mut input);
            multiply_into(&hanning_window, input.as_mut_slice());
            template_plan.r2c(
                input.as_mut_slice(),
                &mut template_vecs[search_num][i*window_size..(i+1)*window_size]
            ).unwrap();
        }
    }

    
}*/

fn step1(num_searches:usize) {
    let args: Vec<String> = env::args().collect();

    let num_args = 1 + 2*num_searches;
    
    if args.len() != num_args+1 {
        panic!("Bruh, gotta provide exactly {} args, haystack, {{needle, output,}}+", num_args);
    }
    
    eprintln!("{:#?}", args);

    let (h_sr, h_rr) = open_file(args[1].clone());

    let mut needle_files = Vec::new();
    for i in 0..num_searches {
        let (sr, rr) = open_file(args[2+(i*2)].clone());
        if sr != h_sr {
            panic!("needles must have same sample rate as haystack");
        }
        needle_files.push(rr);
    }

    // Window size should be a minimum of 512, and I'd be more comfortable at 2048
    let window_size = 2048; //256;
    let overlap = window_size/2;//window_size/2; //(window_size-64);

    let mut needle_sample_vecs = Vec::new();
    for f in needle_files {
        needle_sample_vecs.push(read_file(f));
    }


    let mut all_same = true;
    for vec in &needle_sample_vecs {
        all_same = all_same && vec.len() == needle_sample_vecs[0].len()
    }
    if !all_same {
        panic!("all needles must be same length");
    }
    let haystack_samples = read_file(h_rr);

    eprintln!("{}", haystack_samples.len()*4);

    let needle_num_samples = needle_sample_vecs[0].len();

    if needle_num_samples < window_size {
        panic!("window size too big");
    }
    
    let needle_num_windows   = ( (needle_num_samples-window_size)/(window_size-overlap) ) + 1;
    let haystack_num_windows = ( (haystack_samples.len()-window_size)/(window_size-overlap) ) + 1;

    let hanning_window = hanning(window_size);

    let mut template_vecs = Vec::new();
    for _ in 0..num_searches {
        template_vecs.push(fftw::array::AlignedVec::<fftw::types::c32>::new(needle_num_windows*window_size));
    }

    let mut template_vecs_fine:Vec<fftw::array::AlignedVec<fftw::types::c32>> = Vec::new();
    for _ in 0..num_searches {
        template_vecs_fine.push(fftw::array::AlignedVec::<fftw::types::c32>::new((needle_num_samples - window_size + 1)*window_size));
    }


    let mut input = fftw::array::AlignedVec::<f32>::new(window_size);

    let shape = vec![window_size];
    let mut template_plan = fftw::plan::R2CPlan32::new(
        &shape,
        input.as_slice_mut(),
        &mut template_vecs[0].as_slice_mut()[0..window_size],
        Flag::Patient | Flag::DestroyInput
    ).unwrap();

    for search_num in 0..num_searches {
        for i in 0..needle_num_windows {
            let start_idx = i*(window_size-overlap);
            let end_idx = start_idx + window_size;
            //&needle_sample_vecs[search_num][start_idx..end_idx].clone_into(input.as_slice_mut());
            input.as_slice_mut().copy_from_slice(&needle_sample_vecs[search_num][start_idx..end_idx]);
            multiply_into(&hanning_window, input.as_slice_mut());
            template_plan.r2c(
                input.as_slice_mut(),
                &mut template_vecs[search_num].as_slice_mut()[i*window_size..(i+1)*window_size]
            ).unwrap();
        }
    }
        
    let num_results = (haystack_num_windows-needle_num_windows)+1;

    let mut result_vecs = Vec::new();
    for _ in 0..num_searches {
        result_vecs.push(fftw::array::AlignedVec::<f32>::new(num_results));
    }
    let mut fine_result_vecs = Vec::new();
    for _ in 0..num_searches {
        //fine_result_vecs.push(make_array::<f32>(haystack_samples.len()));
        //TODO: figure out size of vecs so that they can be AlignedVec's
        fine_result_vecs.push(Vec::new():Vec<(usize,Vec<f32>)>);
    }
    
    let mut pbar = ProgressBar::on(std::io::stderr(), ((num_results*num_searches)/1024) as u64);
    let mut pbar_num = 1;

    let num_threads = 1;

    let (results_tx, results_rx) = std::sync::mpsc::channel();
    let (maxs_tx, maxs_rx) = std::sync::mpsc::channel();

    let num_maxs = 10usize;
    let excl = (44100/(window_size-overlap))+1;
    let mut maxs = (0..num_searches).map(|_| Vec::<(usize, f32)>::with_capacity(num_maxs*num_threads)).collect():Vec<_>;
    
    let work_mutex = std::sync::Mutex::new(Vec::new());
    
    crossbeam::scope(|scope| {
        for thread_number in 0..num_threads {
            let start_haystack_number; //inclusive
            if thread_number == 0 {
                start_haystack_number = 0
            } else {
                start_haystack_number = thread_number*(haystack_num_windows/8) - needle_num_windows
            }
            let end_haystack_number; //exclusive
            if thread_number == num_threads - 1 {
                end_haystack_number = haystack_num_windows
            } else {
                end_haystack_number = (thread_number + 1)*(haystack_num_windows/8)
            }

            let results_tx = results_tx.clone();
            let maxs_tx = maxs_tx.clone();
            let hanning_window = &hanning_window;
            let template_vecs = template_vecs.iter().map(|a| a.as_slice()).collect():Vec<_>;
            let haystack_samples = &haystack_samples;
            //let window_size = &window_size;
            //let overlap = &overlap;
            scope.spawn(move |_| {
                let mut haystack_transforms = fftw::array::AlignedVec::<fftw::types::c32>::new(needle_num_windows*window_size);
                let mut haystack_transform_i = 0;
                let mut input = fftw::array::AlignedVec::<f32>::new(window_size);

                let mut maxs = (0..num_searches).map(|_| Vec::<(usize, f32)>::with_capacity(num_maxs+1)).collect():Vec<_>;

                let shape = vec![window_size];
                let mut plan = fftw::plan::R2CPlan32::new(
                    &shape,
                    input.as_slice_mut(),
                    &mut haystack_transforms.as_slice_mut()[0..window_size],
                    Flag::Patient | Flag::DestroyInput
                ).unwrap();
                for i in start_haystack_number..end_haystack_number {
                    let start_idx = i*(window_size-overlap);
                    let end_idx = start_idx + window_size;
                    let hti = haystack_transform_i % needle_num_windows;
                    //&haystack_samples[start_idx..end_idx].clone_into(&mut input);
                    input.as_slice_mut().copy_from_slice(&haystack_samples[start_idx..end_idx]);
                    multiply_into(hanning_window, input.as_slice_mut());
                    plan.r2c(
                        input.as_slice_mut(),
                        &mut haystack_transforms.as_slice_mut()[hti*window_size..(hti+1)*window_size]
                    ).unwrap();

                    haystack_transform_i += 1;
                    if haystack_transform_i >= needle_num_windows {
                        for snum in 0..num_searches {
                            //let mut difference = simd::f32x4::splat(0.0);
                            let mut difference = fftw::types::c32::new(0.0f32, 0.0f32);
                            for i in 0..needle_num_windows {
                                let template_transform = &template_vecs[snum][i*window_size..(i+1)*window_size];
                                let haystack_i = (i + haystack_transform_i) % needle_num_windows;
                                let haystack_transform = &haystack_transforms.as_slice_mut()[haystack_i*window_size..(haystack_i+1)*window_size];
                                for i in 0..window_size {
                                    //difference += (template_transform[i].re - haystack_transform[i].re).abs();
                                    difference += template_transform[i].conj() * haystack_transform[i]; //A cross-correlation, hopefully
                                }
                                /*for thing in 0..(window_size/4) {
                                    
                                    let template = simd::f32x4::load(complex_as_f32(template_transform),thing*4);
                                    let haystack = simd::f32x4::load(complex_as_f32(haystack_transform),thing*4);

                                    //let min = template.min(haystack);
                                    //let max = template.max(haystack);
                                    let mut sub_diff = template - haystack;
                                    sub_diff = sub_diff * sub_diff;
                                    for fekw in 0usize..4usize {
                                        let val = sub_diff.extract(fekw as u32);
                                        println!("{} {} {}", val, thing*4+fekw, snum);
                                    }
                                    difference = difference + sub_diff//(max - min);
                                    
                                    
                                }*/
                            }

                            /*let mut arr = make_array::<f32>(4);
                            difference.store(&mut arr[..], 0);
                            let complexes = f32_as_complex(&arr[..]);
                            let diff_sum =
                                complexes[0].re +
                                complexes[0].im +
                                complexes[1].re +
                            complexes[1].im;*/
                            let diff_sum = difference.to_polar().0; //get magnitude
                            /*let diff_sum =
                                difference.extract(0) +
                                difference.extract(1) +
                                difference.extract(2) +
                                difference.extract(3);*/
                            //if diff_sum > max { max = diff_sum };
                            let global_i = i+1-needle_num_windows;
                            results_tx.send((snum,global_i,diff_sum)).unwrap();
                            let thing = diff_sum;
                            match (&mut maxs[snum]).into_iter().find(|v| (v.0 - excl) < global_i && global_i < (v.0 + excl)) {
                                Some(val) => {
                                    if thing > val.1 {
                                        *val = (global_i, thing);
                                    }
                                },
                                None => {
                                    if (&maxs[snum]).into_iter().any(|v| thing > v.1) || maxs[snum].len() < num_maxs {
                                        maxs[snum].push((i, thing));
                                    }

                                    if maxs.len() > num_maxs {
                                        maxs[snum].sort_unstable_by(|a,b| a.1.partial_cmp(&b.1).unwrap().reverse());
                                        maxs[snum].pop();
                                    }
                                }
                            }
                        }
                    }

                }
                for snum in 0..num_searches {
                    for (idx, val) in &maxs[snum] {
                        maxs_tx.send((snum, *idx, *val)).unwrap();
                    }
                }
            });
        }
        drop(results_tx);
        drop(maxs_tx);

        while let Ok(res) = results_rx.recv() {
            result_vecs[res.0][res.1] = res.2;
            pbar_num += 1;
            if pbar_num % 1024 == 0 {
                pbar.inc();
            }
        }

        while let Ok((snum, idx, val)) = maxs_rx.recv() {
            maxs[snum].push((idx,val));
        }

        for snum in 0..num_searches {
            maxs[snum].sort_unstable_by(|a,b| a.0.cmp(&b.0));
            let mut i = 0;
            while i+1 < maxs[snum].len() {
                if maxs[snum][i+1].0 - maxs[snum][i].0 < excl { //if difference in indexes...
                    if maxs[snum][i].1 > maxs[snum][i+1].1 {
                        maxs[snum].remove(i+1);
                    } else {
                        maxs[snum].remove(i);
                    }
                    // *dont* increment i
                } else {
                    i += 1;
                }
            }
            maxs[snum].sort_unstable_by(|a,b| a.1.partial_cmp(&b.1).unwrap().reverse());
            maxs[snum].truncate(num_maxs);
        }
        dbg!(&maxs[0]);
    }).unwrap();

    crossbeam::scope(|scope| {
        let old_window_size = window_size;
        let old_overlap = overlap;
        let old_needle_num_windows = needle_num_windows;
        let overlap = window_size - 4;
        let needle_num_windows = ( (needle_num_samples-window_size)/(window_size-overlap) ) + 1;
        //let needle_num_windows = needle_num_samples - window_size + 1;
        //let mut template_vecs = template_vecs_fine;

        let ratio = 32.0f32;//old_needle_num_windows as f32 / needle_num_windows as f32;
        dbg!(ratio);


        let mut input = fftw::array::AlignedVec::<f32>::new(window_size);
        let mut template_plan = fftw::plan::R2CPlan32::new(
            &shape,
            input.as_slice_mut(),
            &mut template_vecs_fine[0].as_slice_mut()[0..window_size],
            Flag::Patient | Flag::DestroyInput,
        ).unwrap();

        for snum in 0..num_searches {
            for i in 0..needle_num_windows {
                let start_idx = i*(window_size-overlap);
                let end_idx = start_idx + window_size;
                //&needle_sample_vecs[snum][start_idx..end_idx].clone_into(&mut input);
                input.as_slice_mut().copy_from_slice(&needle_sample_vecs[snum][start_idx..end_idx]);
                multiply_into(&hanning_window, input.as_slice_mut());
                template_plan.r2c(
                    input.as_slice_mut(),
                    &mut template_vecs_fine[snum].as_slice_mut()[i*window_size..(i+1)*window_size],
                ).unwrap();
            }
        }

        let (results_tx, results_rx) = std::sync::mpsc::channel();

        let work_count;
        {
            let mut work = work_mutex.lock().unwrap();
            for snum in 0..num_searches {
                for (idx, _) in &maxs[snum] {
                    work.push((snum, idx));
                }
            }
            dbg!(&work);
            work_count = work.len();
        }

        let peak_width_samples = 1;//1024;//8192;
        for _ in 0..num_threads {
            let work_mutex = &work_mutex;
            let shape = &shape;
            let hanning_window = &hanning_window;
            let template_vecs = template_vecs_fine.iter().map(|a| a.as_slice()).collect():Vec<_>;
            let haystack_samples = &haystack_samples;
            let results_tx = results_tx.clone();
            scope.spawn(move |_| {
                loop {
                    let snum;
                    let idx;
                    {
                        let mut lock = work_mutex.lock().unwrap();
                        match lock.pop() {
                            None => return,
                            Some(el) => {
                                snum = el.0;
                                idx = el.1;
                            },
                        }
                    }

                    //ahead by 7200 for some reason
                    let sample_idx = (idx*(old_window_size-old_overlap))-7200;//-needle_num_samples;
                    let start_sample = max(sample_idx - peak_width_samples, 0);
                    let end_sample = min(sample_idx + peak_width_samples + needle_num_windows, haystack_samples.len() - window_size);
                
                    let mut haystack_transforms = fftw::array::AlignedVec::<fftw::types::c32>::new(needle_num_windows*window_size);
                    let mut haystack_transform_i = 0;
                    let mut input = fftw::array::AlignedVec::<f32>::new(window_size);
                    let mut plan = fftw::plan::R2CPlan32::new(
                        &shape,
                        input.as_slice_mut(),
                        &mut haystack_transforms.as_slice_mut()[0..window_size],
                        Flag::Patient | Flag::DestroyInput,
                    ).unwrap();

                    dbg!(start_sample, end_sample);
                    for i in start_sample..end_sample {
                        let start_idx = i;//*(window_size-overlap);
                        let end_idx = start_idx + window_size;
                        let hti = haystack_transform_i % needle_num_windows;
                        //&haystack_samples[i..i+window_size].clone_into(&mut input);
                        //dbg!(input.len(), haystack_samples.len(), start_idx, end_idx);
                        input.as_slice_mut().copy_from_slice(&haystack_samples[start_idx..end_idx]);
                        //dbg!();
                        multiply_into(hanning_window, input.as_slice_mut());
                        //dbg!();
                        plan.r2c(
                            input.as_slice_mut(),
                            &mut haystack_transforms.as_slice_mut()[hti*window_size..(hti+1)*window_size],
                        ).unwrap();

                        haystack_transform_i += 1;
                        if haystack_transform_i >= needle_num_windows {
                            let mut difference = fftw::types::c32::new(0.0f32, 0.0f32);
                            //dbg!();
                            for i in 0..needle_num_windows {
                                let template_transform = &template_vecs[snum][i*window_size..(i+1)*window_size];
                                let haystack_i = (i + haystack_transform_i) % needle_num_windows;
                                let haystack_transform = &haystack_transforms.as_slice()[haystack_i*window_size..(haystack_i+1)*window_size];
                                for i in 0..window_size {
                                    difference += template_transform[i].conj() * haystack_transform[i];
                                }
                            }


                            let diff_sum = difference.to_polar().0*ratio; //mag
                            results_tx.send((snum, i-window_size, diff_sum)).unwrap();
                        }
                    }
                }
            });
        }
        drop(results_tx);

        dbg!();
        let result_count:u64 = (work_count*peak_width_samples*2) as u64;
        let progress_every:u64 = 256;
        let mut pbar_fine = ProgressBar::on(std::io::stderr(), result_count/progress_every);
        let mut iter_count = 0u64;
        while let Ok((snum, i, diff_sum)) = results_rx.recv() {
            iter_count += 1;
            //fine_result_vecs[snum][i] = diff_sum;
            let mut found = false;
            for (start_idx_mut, elements) in &mut fine_result_vecs[snum] {
                let start_idx = *start_idx_mut;
                let end_idx = start_idx + elements.len();
                let range = (start_idx-peak_width_samples)..(end_idx+peak_width_samples);
                if i == 1000 {
                    dbg!(&start_idx, &end_idx, &range, elements.len());
                }
                if range.contains(&i) {
                    found = true;
                    if i < start_idx {
                        //slow, but should happen rarely
                        let diff = start_idx - i;
                        for _ in 0..diff {
                            elements.insert(0, Default::default());
                        }
                        *start_idx_mut -= diff;
                        elements[0] = diff_sum;
                    } else if start_idx <= i && i < end_idx {
                        let inner_idx = i - start_idx;
                        elements[inner_idx] = diff_sum;
                    } else { // i >= end_idx
                        let diff = (end_idx - i) + 1;
                        for _ in 0..diff {
                            elements.push(Default::default());
                        }
                        let inner_idx = i - start_idx;
                        elements[inner_idx] = diff_sum;
                    }
                    break;
                }
            }

            if !found {
                let new_vec = vec![diff_sum];
                fine_result_vecs[snum].push((i, new_vec));
            }
            if iter_count == 1{
                dbg!(&fine_result_vecs);
            }
            if iter_count == 1000{
                for thing in &fine_result_vecs {
                    dbg!();
                    for (start_idx, elements) in thing {
                        dbg!(start_idx, elements.len());
                    }
                }
            }
            if iter_count % progress_every == 0 {
                pbar_fine.inc();
            }
        }
        dbg!(iter_count);
    }).unwrap();





    let spec = hound::WavSpec {
        channels: 1,
        sample_rate: 44100,
        bits_per_sample: 32,
        sample_format: hound::SampleFormat::Float,
    };

    
    for thing in &fine_result_vecs {
        dbg!();
        for (start_idx, elements) in thing {
            dbg!(start_idx, elements.len());
        }
    }
    
    for snum in 0..num_searches {
        let results = &result_vecs[snum];
        //let fine_results = &fine_result_vecs[snum];
        let mut writer = hound::WavWriter::create(args[3+(snum*2)].clone(), spec).unwrap();

        let mut pbar2 = ProgressBar::on(std::io::stderr(), (haystack_samples.len() as u64)/1024);
        let not_overlap = window_size-overlap;
        for i in 0..(results.len()*not_overlap) {
            let mut samp:Option<f32> = None;
            let mut found = false;
            for (start_idx_borrow, elements) in &fine_result_vecs[snum] {
                let start_idx = *start_idx_borrow;
                let end_idx = start_idx + elements.len();
                if start_idx <= i && i < end_idx {
                    let inner_idx = i - start_idx;
                    //found = true;
                    samp = Some(elements[inner_idx]);
                    break;
                }
            }
            
            if !found {
                samp = Some(results[i/not_overlap]);
            }

            let samp = samp.unwrap();
            /*let samp = if fine_results[i] != f32::default() {
                fine_results[i]
            } else {
                results[i/not_overlap]
            };*/
            writer.write_sample(samp/2.0).unwrap();
            if i % 1024 == 0 {
                pbar2.inc();
            }
        }
        /*for res in results {
            for inner in 0..(window_size-overlap) {
                writer.write_sample(res/10.0).unwrap();
            }
            pbar2.inc();
        }*/
        pbar2.finish();
    }
}
