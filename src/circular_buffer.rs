#[derive(Debug)]
struct CircularBuffer<T> {
    inner:Vec<(bool,Option<T>)>,
    size:usize,
    filled_from:usize,
    filled_to:usize
}

impl<T> CircularBuffer<T> {
    fn new(size:usize) -> Self {
        let inner = Vec::with_capacity(size);
        for _ in 0..size {
            inner.push((false, None));
        }
        Self {
            inner,
            size,
            filled_from: 0usize,
            filled_to: 0usize
        }
    }

    fn len(&self) -> usize {
        if filled_from == filled_to {
            return 0
        } else if filled_from < filled_to {
            return filled_to - filled_from
        } else { // filled_from > filled_to
            return (inner.len - filled_from) + filled_to
        }
    }

    fn get(&self,idx:usize) -> Result<T,()> {

    }

    fn push(&mut self,val:T) -> Result<(),()> {
        if (self.filled_from+1)%inner == self.filled_to {
            return Err(());
        }
        inner[self.filled_to] = (true, Some(val));
        self.filled_to += 1;
    }
}
            
