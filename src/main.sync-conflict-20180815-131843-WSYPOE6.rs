#![feature(toowned_clone_into)]
#![allow(dead_code)]

extern crate fftw;
extern crate audrey;
extern crate pbr;
extern crate crossbeam;
extern crate spmc;
extern crate simd;

use std::env;
use std::vec::Vec;
use std::ops::Range;

use fftw::types::Flag;
use fftw::plan::R2CPlan;

use pbr::ProgressBar;

use audrey::hound;

fn complex_as_f32(cslice: &[fftw::types::c32]) -> &[f32] {
    use std::slice;
    return unsafe { slice::from_raw_parts(cslice.as_ptr() as *const f32, cslice.len() * 2) };
}

fn make_array<T:Default>(size:usize) -> Vec<T> {
    let mut res = Vec::<T>::with_capacity(size);
    for _ in 0..size {
        res.push(Default::default());
    }
    return res;
}

fn open_file(filename:String) -> (u32,audrey::read::BufFileReader) {
    let reader = audrey::read::open(filename)
        .expect("Could not read file");

    let description = reader.description();

    eprintln!("{:#?}", description);

    if description.channel_count() != 1 {
        panic!("Must use an audio file with exactly one channel");
    }

    return (description.sample_rate(), reader);
}

fn read_file(mut reader:audrey::read::BufFileReader) -> Vec<f32> {
    //let mut reader = open_file(filename);

    let samples:Vec<f32> = reader.samples::<f32>().map(|x| x.unwrap()).collect();

    return samples;
}

fn main() {
    //let args: Vec<String> = env::args().collect();

    match env::var("WHICH_STEP") {
        Err(env::VarError::NotPresent) => panic!(),
        Ok(step) => {
            if step == String::from("step1.1") {
                step1(1)
            } else if step == String::from("step1.2") {
                step1(2)
            } else if step == String::from("step2") {
                step2()
            } else if step == String::from("step3") {
                step3()
            } else {
                panic!("thing {:#?}", step);
            }
        },
        val => panic!("unrecognized {:#?}", val),
    }
    
    /*
    if args[0] == String::from("step1") { step1() }
    else if args[0] == String::from("step2") { step2() }
    else {panic!("bad name")}~*/
}

fn step2() {
    let args: Vec<String> = env::args().collect();
    let mut reader = hound::WavReader::open(args[1].clone()).unwrap();

    let mut iter = reader.samples::<f32>();
    let mut i = 0;
    let mut minimum = (i,iter.next().unwrap().unwrap());
    i += 1;
    for r in iter {
        let s = r.unwrap();
        if s < minimum.1 {
            minimum = (i,s)
        }
        i += 1;
    }
    println!("min {:#?}", minimum);
}

fn find_min_excl<T:PartialOrd<T>>(
    within:&[T],
    excl:&mut [Range<usize>]
) -> usize {
    let mut min:Option<(usize,&T)> = None;
    excl.sort_by_key(|r| r.start);
    let mut nums = vec![0usize];
    for r in excl {
        nums.push(r.start);
        nums.push(r.end);
    }
    nums.push(within.len());

    let mut ranges_to_search:Vec<Range<usize>> = Vec::new();
    let mut len = 0;
    for slice in nums.chunks(2) {
        ranges_to_search.push(slice[0]..slice[1]);
        len += slice[1] - slice[0];
    }
    let mut num = 1;
    let mut pbar = ProgressBar::on(std::io::stderr(), (len/1024) as u64);

    for range in ranges_to_search {
        for i in range {
            match min {
                Some((_,old_val)) => {
                    if &within[i] < old_val {
                        min = Some((i, &within[i]))
                    }
                },
                None => min = Some((i, &within[i])),
            }
            num += 1;
            if num % 1024 == 0 {
                pbar.inc();
            }
        }
    }
    
/*    while i < within.len() {
        for range in excl {
            if range.start == i && range.end > range.start {
                let oldi = i;
                i = range.end;
                pbar.add((i-oldi) as u64);
                continue;
            }
        }
    }*/
    pbar.finish();
    match min {
        None => panic!("excluded entire array"),
        Some((i,_)) => return i,
    }
}

fn step3() {
    let args: Vec<String> = env::args().collect();
    let input_podcast_fn = args[3].clone();
    let output_podcast_fn = args[4].clone();
    let mut reader = hound::WavReader::open(args[1].clone()).unwrap();
    let mut other_reader = hound::WavReader::open(args[2].clone()).unwrap();
    let sr = reader.spec().sample_rate as usize;
    let samples:Vec<f32> = reader.samples::<f32>().map(|r| r.unwrap()).collect();
    let other_samples:Vec<f32> = other_reader.samples::<f32>().map(|r| r.unwrap()).collect();
    eprintln!("Got samples");
    let mut exclusions:Vec<std::ops::Range<usize>> = Vec::new();

    let min1 = find_min_excl(&samples, &mut exclusions);
    exclusions.push(min1-(sr)..min1+(sr));
    eprintln!("got min1");
    
    let min2 = find_min_excl(&samples, &mut exclusions);
    exclusions.push(min2-(sr)..min2+(sr));
    eprintln!("got min2");
    
    let min3 = find_min_excl(&samples, &mut exclusions);
    exclusions.push(min3-(sr)..min3+(sr));
    eprintln!("got min3");

    let min4 = find_min_excl(&samples, &mut exclusions);

    eprintln!(
        "using {} {} {}, ignoring {}",
        min1 as f32/sr as f32,
        min2 as f32/sr as f32,
        min3 as f32/sr as f32,
        min4 as f32/sr as f32,
    );

    let mut o_exclusions:Vec<std::ops::Range<usize>> = Vec::new();
    let o_min1 = find_min_excl(&other_samples, &mut o_exclusions);
    o_exclusions.push(o_min1-(sr)..o_min1+(sr));
    eprintln!("got o_min1");
    
    let o_min2 = find_min_excl(&other_samples, &mut o_exclusions);
    o_exclusions.push(o_min2-(sr)..o_min2+(sr));
    eprintln!("got o_min2");
    
    let o_min3 = find_min_excl(&other_samples, &mut o_exclusions);
    o_exclusions.push(o_min3-(sr)..o_min3+(sr));
    eprintln!("got o_min3");

    let o_min4 = find_min_excl(&other_samples, &mut o_exclusions);

    eprintln!(
        "using {} {} {}, ignoring {}",
        o_min1 as f32/sr as f32,
        o_min2 as f32/sr as f32,
        o_min3 as f32/sr as f32,
        o_min4 as f32/sr as f32,
    );

    let ad_start_len_samples = 38525;
    let asls = ad_start_len_samples;

    let mut secs = vec![min1, min2, min3, o_min1, o_min2, o_min3];
    secs.sort_unstable();

    eprintln!("sex {:?}", secs);

    let (_ipsr,mut in_podcast_reader) = open_file(input_podcast_fn);
    let spec = hound::WavSpec {
        channels: 1,
        sample_rate: 44100,
        bits_per_sample: 32,
        sample_format: hound::SampleFormat::Float,
    };
    let mut writer = hound::WavWriter::create(output_podcast_fn, spec).unwrap();
    let mut i = 0;
    for maybe_sample in in_podcast_reader.samples::<f32>() {
        let sample = maybe_sample.unwrap();

        if  (i > secs[0]+asls && i < secs[1]) ||
            (i > secs[2]+asls && i < secs[3]) ||
            (i > secs[4]+asls && i < secs[5])
        {
            //skip
        }else{
            writer.write_sample(sample*256.0).unwrap();
        }

        i += 1;
    }
}

const PI:f32 = 3.141592653;

fn hanning(size:usize) -> Vec<f32>{
    let mut res:Vec<f32> = Vec::with_capacity(size);
    for n in 0..size {
        res.push(
            (
                1.0-( ((n as f32)*2.0*PI)/(size - 1) as f32 ).cos()
            )/2.0
        );
    }
    return res;
}

fn multiply_into(hanning:&[f32], into:&mut [f32]){
    if hanning.len() != into.len() {
        panic!("into must be same len");
    }
    for i in 0..hanning.len() {
        into[i] *= hanning[i];
    }
}

fn step1(num_searches:usize) {
    let args: Vec<String> = env::args().collect();

    let num_args = 1 + 2*num_searches;
    
    if args.len() != num_args+1 {
        panic!("Bruh, gotta provide exactly {} args, haystack, {{needle, output,}}+", num_args);
    }
    
    eprintln!("{:#?}", args);

    let (h_sr, h_rr) = open_file(args[1].clone());

    let mut needle_files = Vec::new();
    for i in 0..num_searches {
        let (sr, rr) = open_file(args[2+(i*2)].clone());
        if sr != h_sr {
            panic!("needles must have same sample rate as haystack");
        }
        needle_files.push(rr);
    }

    let window_size = 256;
    let overlap = 240;

    let mut needle_sample_vecs = Vec::new();
    for f in needle_files {
        needle_sample_vecs.push(read_file(f));
    }


    let mut all_same = true;
    for vec in &needle_sample_vecs {
        all_same = all_same && vec.len() == needle_sample_vecs[0].len()
    }
    if !all_same {
        panic!("all needles must be same length");
    }
    let haystack_samples = read_file(h_rr);

    eprintln!("{}", haystack_samples.len()*4);

    let needle_num_samples = needle_sample_vecs[0].len();

    if needle_num_samples < window_size {
        panic!("window size too big");
    }
    
    let needle_num_windows   = ( (needle_num_samples-window_size)/(window_size-overlap) ) + 1;
    let haystack_num_windows = ( (haystack_samples.len()-window_size)/(window_size-overlap) ) + 1;

    let hanning_window = hanning(window_size);

    let mut template_vecs = Vec::new();
    for _ in 0..num_searches {
        template_vecs.push(make_array::<fftw::types::c32>(needle_num_windows*window_size));
    }

    let mut input = make_array::<f32>(window_size);

    let shape = vec![window_size];
    let mut template_plan = fftw::plan::R2CPlan32::new(
        &shape,
        input.as_mut_slice(),
        &mut template_vecs[0][0..window_size],
        Flag::Patient | Flag::DestroyInput
    ).unwrap();

    for search_num in 0..num_searches {
        for i in 0..needle_num_windows {
            let start_idx = i*(window_size-overlap);
            let end_idx = start_idx + window_size;
            &needle_sample_vecs[search_num][start_idx..end_idx].clone_into(&mut input);
            multiply_into(&hanning_window, input.as_mut_slice());
            template_plan.r2c(
                input.as_mut_slice(),
                &mut template_vecs[search_num][i*window_size..(i+1)*window_size]
            ).unwrap();
        }
    }
        
    let num_results = (haystack_num_windows-needle_num_windows)+1;

    let mut result_vecs = Vec::new();
    for _ in 0..num_searches {
        result_vecs.push(make_array::<f32>(num_results));
    }
    let mut pbar = ProgressBar::on(std::io::stderr(), ((num_results*num_searches)/1024) as u64);
    let mut pbar_num = 1;

    let num_threads = 4;

    let (results_tx, results_rx) = std::sync::mpsc::channel();

    crossbeam::scope(|scope| {
        for thread_number in 0..num_threads {
            let start_haystack_number; //inclusive
            if thread_number == 0 {
                start_haystack_number = 0
            } else {
                start_haystack_number = thread_number*(haystack_num_windows/8) - needle_num_windows
            }
            let end_haystack_number; //exclusive
            if thread_number == num_threads - 1 {
                end_haystack_number = haystack_num_windows
            } else {
                end_haystack_number = (thread_number + 1)*(haystack_num_windows/8)
            }

            let results_tx = results_tx.clone();
            let hanning_window = &hanning_window;
            let template_vecs = &template_vecs;
            let haystack_samples = &haystack_samples;
            //let window_size = &window_size;
            //let overlap = &overlap;
            scope.spawn(move || {
                let mut haystack_transforms = make_array::<fftw::types::c32>(needle_num_windows*window_size);
                let mut haystack_transform_i = 0;
                let mut input = make_array::<f32>(window_size);

                let shape = vec![window_size];
                let mut plan = fftw::plan::R2CPlan32::new(
                    &shape,
                    input.as_mut_slice(),
                    &mut haystack_transforms[0..window_size],
                    Flag::Patient | Flag::DestroyInput
                ).unwrap();
                for i in start_haystack_number..end_haystack_number {
                    let start_idx = i*(window_size-overlap);
                    let end_idx = start_idx + window_size;
                    let hti = haystack_transform_i % needle_num_windows;
                    &haystack_samples[start_idx..end_idx].clone_into(&mut input);
                    multiply_into(hanning_window, input.as_mut_slice());
                    plan.r2c(
                        input.as_mut_slice(),
                        &mut haystack_transforms[hti*window_size..(hti+1)*window_size]
                    ).unwrap();

                    haystack_transform_i += 1;
                    if haystack_transform_i >= needle_num_windows {
                        for snum in 0..num_searches {
                            let mut difference = simd::f32x4::splat(0.0);
                            for i in 0..needle_num_windows {
                                let template_transform = &template_vecs[snum][i*window_size..(i+1)*window_size];
                                let haystack_i = (i + haystack_transform_i) % needle_num_windows;
                                let haystack_transform = &haystack_transforms[haystack_i*window_size..(haystack_i+1)*window_size];
                                /*for i in 0..window_size {
                                    difference += (template_transform[i].re - haystack_transform[i].re).abs();
                                }*/
                                for thing in 0..(window_size/4) {
                                    let template = simd::f32x4::load(complex_as_f32(template_transform),thing*4);
                                    let haystack = simd::f32x4::load(complex_as_f32(haystack_transform),thing*4);

                                    let min = template.min(haystack);
                                    let max = template.max(haystack);
                                    difference = difference + (max - min);
                                }
                            }

                            let diff_sum =
                                difference.extract(0) +
                                //difference.extract(1) +
                                difference.extract(2);// +
                                //difference.extract(3);
                            results_tx.send((snum,i+1-needle_num_windows,diff_sum)).unwrap();
                        }
                    }

                }
            });
        }
        drop(results_tx);

        while let Ok(res) = results_rx.recv() {
            result_vecs[res.0][res.1] = res.2;
            pbar_num += 1;
            if pbar_num % 1024 == 0 {
                pbar.inc();
            }
        }
    });





    let spec = hound::WavSpec {
        channels: 1,
        sample_rate: 44100,
        bits_per_sample: 32,
        sample_format: hound::SampleFormat::Float,
    };

    for snum in 0..num_searches {
        let results = &result_vecs[snum];
        let mut writer = hound::WavWriter::create(args[3+(snum*2)].clone(), spec).unwrap();

        let mut pbar2 = ProgressBar::on(std::io::stderr(), results.len() as u64);
        for res in results {
            for _ in 0..(window_size-overlap) {
                writer.write_sample(res/10.0).unwrap();
            }
            pbar2.inc();
        }
    }
}
