import numpy as np
from mpl_toolkits.mplot3d import Axes3D # This import has side effects required for the kwarg projection='3d' in the call to fig.add_subplot
import matplotlib.pyplot as plt
import random

def fun(x, y):
  return x**2 + y

with open('3d-values-small.txt') as f:
    lines = f.readlines()
    myx = [line.split()[0] for line in lines]
    myy = [line.split()[1] for line in lines]
    myz = [line.split()[2] for line in lines]


fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
x = y = np.arange(-3.0, 3.0, 0.05)
X, Y = np.meshgrid(x, y)
zs = np.array([fun(x,y) for x,y in zip(np.ravel(X), np.ravel(Y))])
Z = zs.reshape(X.shape)

#ax.plot_surface(myx, myy, myz)
ax.scatter(myx,myy,myz)

ax.set_xlabel('X Label')
ax.set_ylabel('Y Label')
ax.set_zlabel('Z Label')

plt.show()
